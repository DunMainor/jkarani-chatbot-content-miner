/* eslint-disable class-methods-use-this */
const fs = require('fs')
const _ = require('lodash')
const KRAscrapper = require('./scrapers/KRAscrapper')
const DOJscrapper = require('./scrapers/DOJscrapper')


const source_urls_arr = [
  {
    category_name: 'KRA_FAQS',
    initial_url: 'https://www.kra.go.ke/en/helping-tax-payers/faqs?start=0',
  },
  {
    category_name: 'DOJ_FAQS',
    initial_url: 'https://www.statelaw.go.ke/faqs/',
  },
]
class ScrapingEngine {
  ScrapingEngine() {
    this.getKRAFaqDetails = this.getKRAFaqDetails.bind(this)
  }

  getKRAFaqDetails(index) {
    KRAscrapper.getFaqs(source_urls_arr[index].initial_url).then((faqs) => {
      const arraylized_faqs = faqs.map((faq, indx) => ({
        question: [faq.question],
        answer: faq.answer,
      }))
      fs.writeFileSync(`${__dirname}/results/${source_urls_arr[index].category_name}.json`, JSON.stringify(_.uniqBy(arraylized_faqs, 'answer')))
      return true
    }).catch((err) => {
      console.log('Received an error: ', err)
      return false
    })
  }

  getDOJFaqDetails(index) {
    DOJscrapper.getFaqs(source_urls_arr[index].initial_url).then((faqs) => {
      const arraylized_faqs = faqs.map((faq, indx) => {
        return {
          question: [faq.question],
          answer: faq.answer,
        }
      }
      )
      fs.writeFileSync(`${__dirname}/results/${source_urls_arr[index].category_name}.json`, JSON.stringify(_.uniqBy(arraylized_faqs, 'question')))
      return true
    }).catch((err) => {
      console.log('Received an error: ', err)
      return false
    })
  }
}

module.exports = ScrapingEngine
