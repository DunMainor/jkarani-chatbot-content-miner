#INSTALLATION
1. Open terminal or CMD and clone the repo
```js
git clone https://gitlab.com/DunMainor/jkarani-chatbot-content-miner.git
```

2. CD into the project
```js
cd jkarani-chatbot-content-miner
```

3. Install NPM package Dependencies
```js
npm i
```

4. Run the Miner
```js
node index
```