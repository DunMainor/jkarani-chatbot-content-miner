const fs = require('fs')
const path = require('path')
const ScrapingEngine = require('./src/ScrapingEngine')
const ParaphraseEngine = require('./src/ParaphraseEngine')
const JSON_Generator = require('./src/JSON_Generator')

const KRAinitialDataSource = require('./src/results/KRA_FAQS.json')
const DOJinitialDataSource = require('./src/results/DOJ_FAQS.json')

const KRAextendedDataSource = require('./src/results/extended_kra_faqs.json')
const DOJextendedDataSource = require('./src/results/extended_doj_faqs.json')



const Scrapper = new ScrapingEngine()
const Paraphraser = new ParaphraseEngine()
const JSON_Gen = new JSON_Generator()


// Scrapper.getKRAFaqDetails(0)
// Scrapper.getDOJFaqDetails(1)
Paraphraser.multiRephrase(KRAinitialDataSource, 'kra_faqs').then((response) => {
    // JSON_Gen.generateJSON(DOJextendedDataSource, 'DOJ_QNA_OPTIMIZED')
    JSON_Gen.generateJSON(KRAextendedDataSource, 'KRA_QNA_OPTIMIZED')
    console.log('Rephrasing questions succeeded with status: ', response)
}).catch((error) => {
    console.log('Rephrasing questions failed with status: ', response)
})
